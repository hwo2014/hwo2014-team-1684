#include "protocol.h"
#include <string>

namespace hwo_protocol {

jsoncons::json make_request(const std::string& msg_type, const jsoncons::json& data) {
  jsoncons::json r;
  r["msgType"] = msg_type;
  r["data"] = data;
  return r;
}

jsoncons::json make_join(const std::string& name, const std::string& key) {
  jsoncons::json data;
  data["name"] = name;
  data["key"] = key;
  return make_request("join", data);
}

jsoncons::json make_createRace(const std::string& name, const std::string& key, int track_id) {
  jsoncons::json data, bot_id;
  bot_id["name"] = name;
  bot_id["key"] = key;
  data["botId"] = bot_id;
  switch (track_id) {
    case 0: {
      data["trackName"] = "keimola";
      break;
    }
    case 1: {
      data["trackName"] = "germany";
      break;
    }
  }
  data["carCount"] = 1;
  return make_request("createRace", data);
}

jsoncons::json make_ping() {
  return make_request("ping", jsoncons::null_type());
}

jsoncons::json make_throttle(double throttle) {
  return make_request("throttle", throttle);
}

}  // namespace hwo_protocol
