#include <fstream>
#include <string>

#include "game_logic.h"
#include "protocol.h"

using namespace hwo_protocol;

game_logic::game_logic()
  : action_map{
      { "join", &game_logic::on_join },
      { "yourCar", &game_logic::on_your_car },
      { "gameInit", &game_logic::on_game_init },
      { "gameStart", &game_logic::on_game_start },
      { "carPositions", &game_logic::on_car_positions },
      { "crash", &game_logic::on_crash },
      { "gameEnd", &game_logic::on_game_end },
      { "error", &game_logic::on_error },
      { "turboAvailable", &game_logic::on_turbo_available },
      { "lapFinished", &game_logic::on_lap_finished }
    } {
  log_file.open("logs/run.log", std::ios::out);
  log_file << std::setprecision(5) << std::fixed;

  car_name = "JZOI";
  last_piece = pos_in_piece = 0;
  velocity = last_velocity = 0;
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg) {
  const auto& msg_type = msg["msgType"].as<std::string>();
  const auto& data = msg["data"];
  auto action_it = action_map.find(msg_type);
  if (action_it != action_map.end()) {
    return (action_it->second)(this, data);
  } else {
    std::cout << "Unknown message type:\t" << msg_type << std::endl;
    return { make_ping() };
  }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data) {
  std::cout << "STATUS\tJoined" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& data) {
  std::cout << "STATUS\tCar color: " << data["color"].as<std::string>() << std::endl;
  car_name = data["name"].as<std::string>();
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data) {
  std::cout << "STATUS\tRace started" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data) {
  std::cout << "STATUS\tSomeone crashed" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data) {
  std::cout << "STATUS\tRace ended" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data) {
  std::cout << "ERROR\t" << data.to_string() << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_turbo_available(const jsoncons::json& data) {
  std::cout << "TURBO\t" << data.to_string() << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_lap_finished(const jsoncons::json& data) {
  int lap = data["lapTime"]["lap"].as<int>();
  double lap_time = data["lapTime"]["millis"].as<double>();
  std::cout << "STATUS\tLap" << lap << " Finished\tTime: " << lap_time << std::endl;
  return { make_ping() };
}

void game_logic::_WriteLogs(const jsoncons::json& data) {
/*
  if (pieces[last_piece].has_member("angle")) {
    int lane_id = data["piecePosition"]["lane"]["startLaneIndex"].as<int>();
    double r = pieces[last_piece]["radius"].as<double>();
    r += lanes[lane_id]["distanceFromCenter"].as<double>();

    double v = velocity;
    double slip_angle = data["angle"].as<double>();
    log_file << v << '\t' << r << '\t' << v * v << '\t' << v * v / r << '\t' << slip_angle << '\n';
  }
*/
  log_file << velocity << '\t' << velocity - last_velocity << '\n';
  log_file.flush();
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data) {
  std::cout << "STATUS\tGame Init" << std::endl;
  total_laps = data["race"]["raceSession"]["laps"].as<double>();

  const auto& track = data["race"]["track"];
  pieces = track["pieces"];
  lanes = track["lanes"];

  int counter = 0;
  for (auto i = 0; i < pieces.size(); ++ i, ++ counter) {
    if (pieces[i].has_member("angle")) {
      last_bend_piece = counter;

      double radius = pieces[i]["radius"].as<double>();
      double angle = pieces[i]["angle"].as<double>();
      pieces[i]["length"] = radius * fabs(angle) * PI / 180;
    }
  }
  std::cout << "total_pieces:" << pieces.size() << "\n";

  std::string track_name = track["name"].as<std::string>();
  std::ofstream out_file(("tracks/" + track_name + ".track").c_str());
  out_file << pretty_print(pieces);
  out_file.close();

  return { make_ping() };
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data) {
  throttle = 0.6;
  for (auto i = 0; i < data.size(); ++ i) {
    if (data[i]["id"]["name"] == car_name) { // find my car
      int lap = data[i]["piecePosition"]["lap"].as<int>();
      velocity = _GetVelocity(data[i]);

      // TODO: add strategy

      // If this is the last lap and there're no bends any more, accelerate.
      /*
      if (last_piece > last_bend_piece) {
        throttle = lap + 1 == total_laps ? 1 : 0.8;
      }
      */
      _WriteLogs(data[i]);
    }
  }
  return { make_throttle(throttle) };
}

double game_logic::_GetVelocity(const jsoncons::json& car_data) {
  double result = 0;
  double position = car_data["piecePosition"]["inPieceDistance"].as<double>();
  int current_piece = car_data["piecePosition"]["pieceIndex"].as<int>();

  if (current_piece == last_piece) {
    result = position - pos_in_piece;
  } else {
    result = last_velocity;
  }
  last_piece = current_piece;
  pos_in_piece = position;
  last_velocity = velocity;
  return result;
}
