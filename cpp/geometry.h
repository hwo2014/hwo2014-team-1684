#include <cmath>

const double EPS = 1E-8;
const double PI = acos(-1.0);

struct Point {
  double x, y;
  Point(double _x = 0, double _y = 0) : x(_x), y(_y) {}

  friend Point operator + (const Point &a, const Point &b);
  friend Point operator - (const Point &a, const Point &b);
  friend Point operator * (const Point &a, const double &len);
  friend Point operator / (const Point &a, const double &len);
  friend bool operator == (const Point &a, const Point &b);

  friend double distance(const Point &a, const Point &b);
  friend Point rotate(const Point &source, const Point &centre, const double &angle);
};
