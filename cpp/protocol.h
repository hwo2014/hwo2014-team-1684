#ifndef HWO_PROTOCOL_H_
#define HWO_PROTOCOL_H_

#include <iostream>
#include <string>

#include "jsoncons/json.hpp"

namespace hwo_protocol {

jsoncons::json make_request(const std::string& msg_type, const jsoncons::json& data);
jsoncons::json make_join(const std::string& name, const std::string& key);
jsoncons::json make_createRace(const std:: string& name, const std::string& key, int track_id);
jsoncons::json make_ping();
jsoncons::json make_throttle(double throttle);

}  // namespace hwo_protocol

#endif  // HWO_PROTOCOL_H_
