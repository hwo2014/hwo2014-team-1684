#include "geometry.h"

inline int sgn(const double &x) {
  return x < -EPS ? -1 : x > EPS;
}

inline double sqr(const double &x) {
  return x * x;
}

Point operator + (const Point &a, const Point &b) {
  return Point(a.x + b.x, a.y + b.y);
}

Point operator - (const Point &a, const Point &b) {
  return Point(a.x - b.x, a.y - b.y);
}

Point operator * (const Point &a, const double &len) {
  return Point(a.x * len, a.y * len);
}

Point operator / (const Point &a, const double &len) {
  return Point(a.x / len, a.y / len);
}

bool operator == (const Point &a, const Point &b) {
  return sgn(a.x - b.x) == 0 && sgn(a.y - b.y) == 0;
}

double distance(const Point &a, const Point &b) {
  return sqrt(sqr(a.x - b.x) + sqr(a.y - b.y));
}

Point rotate(const Point &source, const Point &centre, const double &angle) {
  Point vec = source - centre;
  double c = cos(angle);
  double s = sin(angle);
  return Point(centre.x + vec.x * c - vec.y * s, centre.y + vec.x * s + vec.y * c);
}
