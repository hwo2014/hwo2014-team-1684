#ifndef HWO_GAME_LOGIC_H_
#define HWO_GAME_LOGIC_H_

#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <string>
#include <vector>

#include "geometry.h"
#include "jsoncons/json.hpp"

class game_logic {
 public:
  typedef std::vector<jsoncons::json> msg_vector;

  game_logic();
  msg_vector react(const jsoncons::json& msg);

 private:
  typedef std::function<msg_vector(game_logic*, const jsoncons::json&)> action_fun;
  const std::map<std::string, action_fun> action_map;

  msg_vector on_join(const jsoncons::json& data);
  msg_vector on_your_car(const jsoncons::json& data);
  msg_vector on_game_init(const jsoncons::json& data);
  msg_vector on_game_start(const jsoncons::json& data);
  msg_vector on_car_positions(const jsoncons::json& data);
  msg_vector on_crash(const jsoncons::json& data);
  msg_vector on_game_end(const jsoncons::json& data);
  msg_vector on_error(const jsoncons::json& data);
  msg_vector on_turbo_available(const jsoncons::json& data);
  msg_vector on_lap_finished(const jsoncons::json& data);

  std::fstream log_file;
  std::string car_name;
  jsoncons::json pieces, lanes;

  double velocity, last_velocity;      // per tick
  double throttle;
  double pos_in_piece;
  int lane;
  int last_piece;
  int last_bend_piece;
  int total_laps;

  double _GetVelocity(const jsoncons::json& data);
  void _WriteLogs(const jsoncons::json& data);
};

#endif  // HWO_GAME_LOGIC_H_
